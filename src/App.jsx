import React, {useEffect, useState} from 'react';
import {BrowserRouter,Route} from "react-router-dom";
import FilmsList from "./Sections/FilmsList";
import Header from "./Components/Header";
import './scss/index.scss';
import Loading from "./Components/Loading";
import FilmInfo from "./Sections/FilmInfo";
import Search from "./Components/Search";
import {DeleteFilmContext} from "./Contexts/DeleteFilmContext";
import ModalPortal from "./Portals/ModalPortal";
import Modal from "./Components/Modal";


const App = () => {

    const [filmList, setFilmList] = useState([]);
    const [filmListFilter, setFilmListFilter] = useState([]);
    const [filterState,setFilterState] = useState(false);
    const [modalState, setModalState] = useState(false);
    const [modalText, setModalText] = useState('');

    useEffect(() => {
        !filmList.length && fetch('./filmList.json')
            .then(response => response.json())
            .then(result => setFilmList(result));
    },[filmList]);


    const deleteFilm = (title) => {
        const newFilmList = filmList.slice();
        newFilmList.forEach((item,index) => {
            item.Title === title && newFilmList.splice(index,1)
        });
        setFilmList(newFilmList);
        setModalState(true);
        setModalText('The movie has been successfully deleted!');
    };

    const addFilm = (filmData) => {
        const newFilmList = filmList.slice();
        newFilmList.push(filmData);
        setFilmList(newFilmList);
        setModalState(true);
        setModalText('\n' + 'The movie was added successfully!');
    };

    const findParams = (text, type) => {
        setFilterState(Boolean(text));
        const newList = [];
        filmList.forEach((item) => {
            if(type === 'title') {
                item.Title.includes(text) && newList.push(item);
            } else {
                item.Stars.forEach((elem) => elem.includes(text) && newList.push(item));
            }
        });
        setFilmListFilter(newList);
    }

  return (
      <>
          <BrowserRouter>
              <Header filmList={filmList} sendFilmData={addFilm} />
              {modalState && <ModalPortal><Modal closeModal={setModalState}>{modalText}</Modal></ModalPortal>}
              <DeleteFilmContext.Provider value={deleteFilm}>
                  <Route exact path='/'>
                      <Search findParams={findParams} />
                      {filmList && !filterState && <FilmsList filterState={filterState} filmList={filmList}/>}
                      {filterState && <FilmsList filterState={filterState} filmList={filmListFilter} />}
                      {!filmList && <Loading />}
                  </Route>
                  {filmList && filmList.map((elem, index) => {
                      return(
                          <Route key={index} path={`/${elem.Title}`}>
                              <FilmInfo
                                  key={`route-${index}`}
                                  {...elem}
                              />
                          </Route>
                      );
                  })}
              </DeleteFilmContext.Provider>
          </BrowserRouter>
      </>
  );
};


export default App;
