import React, {useContext, useEffect} from 'react';
import {NavLink} from "react-router-dom";
import Button from "../Button";
import {DeleteFilmContext} from "../../Contexts/DeleteFilmContext";


const FilmCard = ({Title, ReleaseYear}) => {

    const deleteFilm = useContext(DeleteFilmContext);

    return(
        <div className='film-card'>
            <h2 className='film-card__title'>{Title}</h2>
            <Button onClick={() => deleteFilm(Title)} className='film-card__delete'><i className="fas fa-trash-alt"> </i></Button>
            <p className='film-card__year'>Release Year: <span className='film-card__year-number'>{ReleaseYear}</span></p>
            <NavLink to={`/${Title}`}><div className='film-card__show-more'>Show More <i className="fas fa-arrow-right"> </i></div></NavLink>
        </div>
    );
};


export default FilmCard;