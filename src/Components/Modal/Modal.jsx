import React from "react";
import Button from "../Button";


const Modal = ({children,closeModal}) => {

    return(
        <div className='modal'>
            {children}
            <Button className='modal__button' onClick={() => closeModal(false)}>Confirm</Button>
        </div>
    );
};


export default Modal;