import React, {useState} from "react";
import Button from "../Button";
import Select from "react-select";

const AddFilm = ({closeModal, sendFilmData, filmList}) => {

    const [title, setTitle] = useState('');
    const [releaseYear, setReleaseYear] = useState('');
    const [format, setFormat] = useState([]);
    const [stars, setStars] = useState([]);
    const [starsInputValue, setStarsInputValue] = useState('');
    const [starsState, setStarsState] = useState(false);
    const [errorState, setErrorState] = useState(false);
    const [yearStateError, setYearStateError] = useState(false);
    const [errorDataState, setErrorDataState] = useState({
        eTitle: false,
        eYear: false,
        eFormat: false,
        eStars: false,
    });
    const Formats = [{value: 1, label: 'DVD'}, {value: 2, label: 'VHS'}, {value: 3, label: 'Blu-ray'},];

    const addStar = () => {
        let unique = true;
        stars.forEach((item) => {
            if(starsInputValue === item) {
                unique = false;
            }
        });
        if(starsInputValue && unique){
            const newStars = stars.slice();
            newStars.push(starsInputValue);
            setStars(newStars);
            setStarsState(false);
        }
    };

    const validationData = () => {
        (releaseYear > 2021 || releaseYear < 1800) ? setYearStateError(true) : setYearStateError(false);
        title ? errorDataState.eTitle = false :  errorDataState.eTitle = true;
        releaseYear ? errorDataState.eYear = false :  errorDataState.eYear = true;
        format.length ? errorDataState.eFormat = false :  errorDataState.eFormat = true;
        stars.length ? errorDataState.eStars = false :  errorDataState.eStars = true;
    };

    const confirmFilm = () => {
        let unique = true;
        filmList.forEach(({Title}) => {
            if(title === Title) {
                unique = false;
            }
            setErrorState(true);
        });
        validationData();
        if(unique && !errorDataState.eTitle && !errorDataState.eYear && !errorDataState.eFormat && !errorDataState.eStars && !yearStateError) {
            setErrorState(false);
            closeModal(false);
            sendFilmData({
                Format: format,
                ReleaseYear: releaseYear,
                Stars: stars,
                Title: title,
            });
        }
    };

    return(
        <div className='add-film'>
            <Button onClick={() => closeModal(false)} className='add-film__close'><i className="fas fa-times"> </i></Button>
            <div className='add-film__input-box'>
                <span className='add-film__label'>Title:</span>
                <input style={{marginLeft: '8%'}} className='add-film__input-text' onChange={({target}) => setTitle(target.value)} value={title} type="text"/>
                {errorDataState.eTitle && <p className='add-film__error-text add-film__error-text-1'>Fill in this field</p>}
            </div>
            <div className='add-film__input-box'>
                <span className='add-film__label'>Release Year:</span>
                <input className='add-film__input-text' onChange={({target}) => (Number(target.value) || target.value === '') && setReleaseYear(target.value)} value={releaseYear} type="text"/>
                {errorDataState.eYear && <p className='add-film__error-text add-film__error-text-1'>Fill in this field</p>}
                {yearStateError && <p className='add-film__error-text add-film__error-text-1 add-film__error-text-year'>Enter the correct year</p>}
            </div>
            <div className='add-film__input-box'>
                <span className='add-film__label'>Format:</span>
                <Select className='add-film__select' options={Formats} onChange={({label}) => setFormat(label)}> </Select>
                {errorDataState.eFormat && <p className='add-film__error-text add-film__error-text-1'>Fill in this field</p>}
            </div>
            <div style={{width: '96%', justifyContent: "center"}} className='add-film__input-box'>
                {!starsState && <span className='add-film__label'>Stars:</span>}
                {starsState && <span style={{marginRight: '39%'}} className='add-film__label'>Stars:</span>}
                {starsState && <div className='add-film__stars-box'>
                    <input className='add-film__input-text' value={starsInputValue} onChange={({target}) => setStarsInputValue(target.value)} type='text'/>
                    <Button className='add-film__confirm' onClick={addStar}>Confirm</Button>
                </div>}
                {!starsState && <ul className='add-film__stars-list'>
                    {stars.length && stars.map((item, index) => {
                        return(
                            <li key={index}>{item}</li>
                        )
                    })}
                </ul>}
                {errorDataState.eStars && <p className='add-film__error-text add-film__error-text-1'>Fill in this field</p>}
                {!starsState && <Button className="add-film__add-star" onClick={() => setStarsState(true)}>Add star</Button>}
            </div>
            <Button onClick={confirmFilm} className="add-film__confirm-1">Confirm</Button>
            <p className='add-film__error-text'>{errorState && 'You cannot add the same title to the movie'}</p>
        </div>
    );
}


export default AddFilm;