import React, {useState} from 'react';
import Button from "../Button";
import ModalPortal from "../../Portals/ModalPortal";
import AddFilm from "../AddFilm";
import {useHistory, useLocation} from "react-router";
import {useSelector} from "react-redux";


const Header = ({sendFilmData, filmList}) => {

    let location = useLocation();
    let history = useHistory();

    const locationText = useSelector(({getLocation}) => getLocation);

    const [modalState, setModalState] = useState(false);

    return(
        <>
            <header className='header'>
                {location.pathname === '/' && <Button onClick={() => setModalState(true)} className='header__add-film'>Add film</Button>}
                {location.pathname !== '/' && <Button onClick={() => history.goBack()} className='header__back'>Back</Button>}
                <h1>{locationText}</h1>
            </header>
            {modalState && <ModalPortal><AddFilm filmList={filmList} sendFilmData={sendFilmData} closeModal={setModalState} /></ModalPortal>}
        </>
    )
};

export default Header;