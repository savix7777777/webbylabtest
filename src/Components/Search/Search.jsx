import React, {useState} from 'react';


const Search = ({findParams}) => {

    const [findText, setFindText] = useState('');
    const [findType, setFindType] = useState('title');

    const findFilm = ({target}) => {
        setFindText(target.value);
        findParams(target.value, findType);
    };

    return(
        <div className='search'>
            <span className='search__find-text'>Find Film:</span> <input type='text' className='search__input-text' onChange={findFilm} value={findText} />
            <div className='search__radio-box'>
                <input onChange={({target}) => setFindType(target.value)} className="custom-radio" name="type" type="radio" id="title" value="title" checked={findType === 'title'} />
                    <label htmlFor="title">Title</label>
                <input onChange={({target}) => setFindType(target.value)} className="custom-radio" name="type" type="radio" id="star" value="star" checked={findType === 'star'} />
                    <label htmlFor="star">Star</label>
            </div>
        </div>
    );
};


export default Search;