import {combineReducers} from "redux";
import getLocation from "./getLocation";

export default combineReducers({
    getLocation,
});