import {CHANGE_LOCATION} from "./action";

export const createLocation = (payload) => {
    return {type: CHANGE_LOCATION, payload}
}