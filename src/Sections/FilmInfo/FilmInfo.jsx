import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {createLocation} from "../../store/actions/actionCreator";


const FilmInfo = ({Title, ReleaseYear, Stars, Format}) => {

    const dispatch = useDispatch();

    const sendLocation = (value) => dispatch(createLocation(value));

    useEffect(() => {
        sendLocation(Title);
    });

    return(
        <section className='film-info'>
            <div>
                <h2 className='film-card__title'>{Title}</h2>
                <p className='film-info__year'>Release Year: <span className='film-card__year-number'>{ReleaseYear}</span></p>
                <p className='film-info__format'>Format: <span className='film-card__year-number'>{Format}</span></p>
            </div>
            <div className='film-info__stars-box'>
                <h2 className='film-info__stars-h'>Stars:</h2>
                <div className='film-info__blue-line'> </div>
                <ul className='film-info__stars-list'>
                    {Stars.map((elem, index) => {
                        return(
                            <li
                                key={index}
                            >{elem}</li>
                        );
                    })}
                </ul>
            </div>
        </section>
    );
};


export default FilmInfo;