import React, {useEffect} from 'react';
import FilmCard from "../../Components/FilmCard";
import {useLocation} from "react-router";
import {createLocation} from "../../store/actions/actionCreator";
import {useDispatch} from "react-redux";


const FilmsList = ({filmList,filterState}) => {

    const dispatch = useDispatch();
    let location = useLocation();

    const sendLocation = (value) => dispatch(createLocation(value));

    useEffect(() => {
        location.pathname === '/' && sendLocation('Film List');
    });

    return(
        <section className='films-list'>
            {filterState && !filmList.length && <div className='films-list__no-matches-box'><h1 className='films-list__no-matches'>No matches :(</h1></div>}
            {filmList && filmList.sort((a,b) => a.Title.localeCompare(b.Title)).map((elem, index) => {
                return(
                    <FilmCard
                        key={index}
                        {...elem}
                    />
                )
            })}
        </section>
    )
}


export default FilmsList;